# Digital Forensics and Incident Response - Ejercicio 1 - Análisis

## Enunciado

El ejercicio consiste en hacer un análisis de una máquina utilizando las herramientas proporcionadas a lo largo del curso. Además es muy importante indicar las herramientas utilizadas y cómo se ha llegado a obtener el resultado.

* Lo primero que se debe hacer es identificar la evidencia, sacar los hashes y demás.
* Después, analizar los diferentes *artifacts*.

Existe la sospecha de que el usuario del equipo está sacando información de la compañía de su equipo y, además, el equipo de monitorización ha levantado una alerta indicando comportamientos extraños en el equipo, por ello nos han llamado para analizar el equipo y determinar qué indicios y evidencias existen sobre las sospechas fundadas.

Las principales respuestas que debemos dar son las siguientes:

* Nombre de la máquina y modelo.
* ¿Podemos saber el password del usuario?
* Usuarios de la máquina.
* Archivos descargados y eliminados.
* Carpetas accedidas recientemente.
* USB conectados a la máquina.
* Logon / logoff del sistema.
* ¿Tiene algún servicio de almacenamiento en cloud?
* ¿Se han producido accesos de forma remota a la máquina?
* ¿Es posible que se haya accedido a documentación de otros equipos?
* ¿Existe algún proceso sospechoso en la máquina?

Estas son algunas pistas, pero cuanto más información se pueda sacar: mejor.

Es necesario indicar con capturas el resultado obtenido y los programas ejecutados, tanto si han dado resultado como si no.

## Solución

### Software

Se han utilizado las siguientes herramientas:

* [Windows](https://www.microsoft.com/es-es/software-download/windows10) 10.
  * [VMWare](https://www.vmware.com/) Workstation 16 Pro 16.2.2 build-19200509.
    * [Tsurugi](https://tsurugi-linux.org/) 2022.1.
      * [Guymager](https://guymager.sourceforge.io/) 0.8.13-beta-tsurugi-1.
  * [VirtualBox](https://www.virtualbox.org/) 6.1.12 r139181 (Qt5.6.2).
    * [Windows](https://developer.microsoft.com/en-us/windows/downloads/virtual-machines/) 10.
      * [Arsenal Image Mounter](https://arsenalrecon.com/downloads/) 3.3.138.
      * [Kape](https://ericzimmerman.github.io/KapeDocs/#!index.md) 1.2.0.0.
      * [AccessData FTK Imager](https://www.python.org/downloads/) 4.2.1.4.
      * [Active@ Disk Editor](https://www.disk-editor.org/index.html) 7 x64.
      * [Registry Explorer](https://ericzimmerman.github.io/) 1.6.0.0.
      * [Timeline Explorer](https://ericzimmerman.github.io/) 1.3.0.0.

### Identificación y descripción de la evidencia

La evidencia proporcionada es un archivo imagen de un disco duro con un sistema operativo Windows 10.

Datos identificativos de la evidencia:

* Archivo imagen de un disco duro con nombre "*Win10_PC001.vmdk*".
* Etiqueta de la evidencia y de sus copias:
  * "*Win10_PC001.vmdk*".
  * "*Win10_PC001-backup.vmdk*"
* Persona encargada de la adquisición:
  * Aurelio Pérez Noriega.
* Datos de la adquisición:
  * Lugar: Keepcoding.
  * Tiempo: 03/05/2022 a las 12:04 horas.
  * Software: imagen de disco con sistema operativo Windows 10.
* Hash de la imagen obtenida:
  * SHA-1: ab7f6987e2f3e7fd800901dc12eba53784e498d6.
  * MD5: 0ad2b998a5dcfb825d01e29b64a46121.
* Lugar donde estaba la evidencia:
  * En el equipo del empleado de la compañía (dato ficticio para la práctica).
* Dónde se ha realizado la copia:
  * En el laboratorio de TI la compañía (dato ficticio para la práctica).
* Datos de la máquina:
  * Sistema Operativo Windows 10.
  * Nombre "PEGASUS01".
  * Usuario principal: "IEUser".
  * Contraseña del usuario principal: "Passw0rd!"

### Copia de seguridad

Realizar una copia de seguridad de la evidencia para ser almacenada en un lugar seguro:

![Backup evidencia](./img/01-backup_evidencia.png)

### Montaje del disco duro en modo de sólo lectura

Montar el disco duro en modo sólo lectura en una máquina virtual Tsurugi, y realizar las siguiente acciones:

* Adquirir una imagen del disco (simulando un caso real, teniendo ya un fichero de imagen no sería necesario)
* Obtener el fichero de información de Guymager "[kc001.info](./kc001.info)".

![Tsurugi adquisición imagen Guymager](./img/02-tsurugi_guymager.png)

### Montaje de la imagen del disco

Preparar la imagen del disco duro en modo de sólo lectura con Arsenal Image Mounter:

![Arsenal Image Mounter](./img/03-arsenal_image_mounter.png)

### Extracción de información

Utilizar la herramienta Kape para extraer información potencialmente útil del disco duro ejecutando la siguiente instrucción:

```powershell
.\kape.exe --tsource E: --tdest "C:\Users\Equipo Pruebas\Desktop\evidencia\target" --tflush --target Avast,AnyDesk,GoogleDrive_Metadata,GoogleDriveBackupSync_UserFiles,OneDrive_Metadata,OneDrive_UserFiles,SupremoRemoteDesktop,TeamViewerLogs,Chrome,ChromeExtensions,ChromeFileSystem,Edge,!BasicCollection,FileSystem,KapeTriage,RecycleBin,RegistryHives,RemoteAdmin,$Boot,$LogFile,$MFT,Amcache,EventLogs,EventLogs-RDP,EventTraceLogs,Prefetch,RDPCache,RDPLogs,RecentFileCache,RecycleBin_DataFiles,RecycleBin_InfoFiles,RegistryHivesOther,RegistryHivesSystem,RegistryHivesUser,StartupFolders,StartupInfo --vss --gui
```

El log de las operaciones está en el arhivo [kape_target_console.log](./kape_target_console.log).

![Kape Targets](./img/04-kape_targets.png)

Utilizar la herramienta Kape para procesar la información previamente extraída ejecutando la siguiente instrucción:

```powershell
.\kape.exe --msource "C:\Users\Equipo Pruebas\Desktop\evidencia\target" --mdest "C:\Users\Equipo Pruebas\Desktop\evidencia\modules" --module MFTECmd,RegRipper,bstrings_UNC,bstrings_URLs,JLECmd,LECmd,PECmd,RBCmd,RecentFileCacheParser,SBECmd,SrumECmd,WxTCmd --ifw --gui
```

El log de las operaciones está en el arhivo [kape_modules_console.log](./kape_modules_console.log).

![Kape Modules](./img/05-kape_modules.png)

### Exploración manual del disco _con "AccessData FTK Imager"

Abrir la imagen de disco con AccessData FTK Imager.

#### Directorio "Users"

El usuario de la máquina es "IEUser":

![FTK Imager Users](./img/06-ftk_imager_users.png)

#### Directorio "Program Files"

Entre el software instalado destacan:

* [Google Drive](https://drive.google.com/).
* [Team Viewer](https://www.teamviewer.com/).

![FTK Imager Program Files](./img/07-ftk_imager_program_files.png)

#### Directorio "Program Data"

Entre los datos del software que ha estado alguna vez instalado destacan:

* [Microsoft One Drive](https://www.microsoft.com/es-es/microsoft-365/onedrive/online-cloud-storage).
* [SSH](https://es.wikipedia.org/wiki/Secure_Shell).

![FTK Imager Program Data](./img/08-ftk_imager_program_data.png)

#### Directorio "TMP"

Existen varios archivos sospechosos en este directorio:

![FTK Imager TMP](./img/09-ftk_imager_tmp.png)

Entre los archivos sospechosos de este directorio destacan:

* ["WMIBackdoor.ps1"](https://www.virustotal.com/gui/file/57eb1179abfb81ee54880287307f0f770eabf5b01e247b9a9789fa70a074c21b) disponible en [GitHub](https://github.com/mattifestation/WMI_Backdoor/blob/master/WMIBackdoor.ps1).
* [xCmd.exe](https://www.virustotal.com/gui/file/6c8eea3ba31463a04d041f4c9ff50b50d9b5945d3306fee35fb4b5bfd292692b).
* [nbtscan.exe](https://www.virustotal.com/gui/file/c9d5dc956841e000bfd8762e2f0b48b66c79b79500e894b4efa7fb9ba17e4e9e).
* [p.exe](https://www.virustotal.com/gui/file/f8dbabdfa03068130c277ce49c60e35c029ff29d9e3c74c362521f3fb02670d5/behavior/C2AE).
* [127.0.0.1.txt](./127.0.0.1.txt).

Podrían ser utilizados para abrir una puerta trasera al equipo que proporcione un *shell*.

![FTK Imager TMP WMIBackdoor.ps1](./img/10-ftk_imager_tmp_wmi_backdoor.png)

#### Directorio "Users\IEUser\AppData\Local\Temp"

Entre los archivos destacados está "BGInfo.bmp":

![FTK Imager Users\IEUser\AppData\Local\Temp BGInfo.bmp](./img/11-ftk_imager_users_ieuser_appdata_local_temp_bginfo.png)

Entre los archivos sospechosos está el directorio "APTSimulator", eliminado, y que fue descomprimido conteniendo el software "[APTSimulator](https://github.com/NextronSystems/APTSimulator)":

![FTK Imager Users\IEUser\AppData\Local\Temp APTSimulator](./img/12-ftk_imager_users_ieuser_appdata_local_temp.png)

#### Directorio "Users\IEUser\AppData\Roaming\TeamViewer"

Entre los archivos destacados está "[TeamViewer15_Logfile.txt](./TeamViewer15_Logfile.txt)", que contiene información relativa a la instalación del software "Team Viewer".

![FTK Imager Users\IEUser\AppData\Roaming TeamViewer](./img/13-ftk_imager_users_ieuser_appdata_roaming_teamviewer.png)

#### Directorio "Users\IEUser\Downloads"

Entre los archivos destacados están "GoogleDriveSetup.exe" y "TeamViewer_Setup_x64.exe":

![FTK Imager Users\IEUser Downloads](./img/14-ftk_imager_users_ieuser_downloads.png)

#### Directorio "Users\IEUser\AppData\Roaming\Microsoft\Windows\Recent"

Entre los archivos abiertos recientemente por el usuario, los más destacados son:

* "Documento Seguridad HipoSEMG.doc.lnk".
* "GINF-G-006 Guia para una Contrasena Segurda.doc.lnk".
* "manualproyectos.pdf.lnk".
* "http--192.168.183.134.lnk" (posiblemente es un acceso remoto a una máquina de la red local).
* "windowsdefender--network-.lnk" (posiblemente acceso a la configuracion de seguridad de la red del equipo).
* "windowsdefender--network-.lnk" (posiblemente acceso a la configuración de seguridad contra virus del equipo).
* "My Drive.lnk" (posiblemente acceso al almacenamiento Cloud de Google Drive).

![FTK Imager Users\IEUser\AppData\Roaming\Microsoft\Windows Recent](./img/15-ftk_imager_users_ieuser_appdata_roaming_microsoft_windows_recent.png)

### Exploración manual del registro de Windows con "Registry Explorer"

Abrir los archivos del directorio "Windows\system32\config" con "Registry Explorer".

#### Registro "SAM"

Las cuentas de usuario existentes en el equipo son:

* Administrator.
* DefaultAccount.
* Guest.
* IEUser.
* sshd (probablemente ejecuta el servicio SSH del software OpenSSH).
* WDAGUtilityAccount.

![Registry Explorer User Accounts](./img/16-registry_explorer_user_accounts.png)

#### Registro "SOFTWARE"

El software instalado en el equipo más destacado es:

* Google Drive.
* OpenSSH.
* TeamViewer.

![Registry Explorer Installed Software](./img/17-registry_explorer_software_installed.png)

### Exploración con "Timeline Explorer" del los resultado de los análisis realizados con Kape

#### Deleted Files

Abrir con "Timeline Explorer" el fichero de análisis [20220521111510_RBCmd_Output.csv](./20220521111510_RBCmd_Output.csv). Los archivos eliminados son:

* "C:\Users\IEUser\Documents\02_AnejoII_EstrucyContTFG_a.pdf".
* **"C:\Users\IEUser\AppData\Local\Temp\cosas.zip"**, este archivo resulta sospechoso.
* "C:\Users\IEUser\Documents\CONFIDENTIAL document list.pdf".
* "C:\Users\IEUser\Documents\Documento Seguridad HipoSEMG.doc".

![Timeline Explorer Deleted Files](./img/18-timeline_explorer_deleted_files.png)

#### Activity

Abrir con "Timeline Explorer" el fichero de análisis [20220521111459_Equipo Pruebas_Activity.csv](./20220521111459_Equipo Pruebas_Activity.csv). Se aprecian las siguientes actividades:

* Ejecución de One Drive.
* Login en Google.
* Descarga e instalación de Google Drive.
* Login con Google Drive.
* Ejecución de Team Viewer.
* Obtención del documento "\\192.168.183.134\Docs\archivos\manualproyectos.pdf" (accediendo a otro equipo de la red local).

![Timeline Explorer Activity](./img/19-timeline_explorer_activity.png)

#### Automatic Destinations

Abrir con "Timeline Explorer" el fichero de análisis [20220521111504_AutomaticDestinations.csv](./20220521111504_AutomaticDestinations.csv). Se aprecian actividades semejantes a las del punto "Activity", con el añadido de que se accede a la configuración de seguridad de "Windows Defender":

![Timeline Explorer Automatic Destinations](./img/20-timeline_explorer_automatic_destinations.png)

#### Custom Destinations

Abrir con "Timeline Explorer" el fichero de análisis [20220521111504_CustomDestinations.csv](./20220521111504_CustomDestinations.csv). Se aprecia que el usuario utiliza el depurador [PowerShell ISE](https://docs.microsoft.com/en-us/powershell/scripting/windows-powershell/ise/introducing-the-windows-powershell-ise?view=powershell-7.2):

![Timeline Explorer Custom Destinations](./img/21-timeline_explorer_custom_destinations.png)

#### LECmd

Abrir con "Timeline Explorer" el fichero de análisis [20220521111505_LECmd_Output.csv](./20220521111505_LECmd_Output.csv). Se aprecian actividades semejantes a las del punto "Activity" y "Automatic Destinations":

![Timeline Explorer LECmd Output](./img/22-timeline_explorer_lecmd.png)

#### NTUSER.DAT

Abrir con "Timeline Explorer" el fichero de análisis [IEUser_NTUSER.csv](./IEUser_NTUSER.csv). Se aprecia el acceso remoto a la ruta de red "\\192.168.183.134\Docs\archivos":

![Timeline Explorer NTUSER.DAT](./img/23-timeline_explorer_ntuser.png)

#### UsrClass

Abrir con "Timeline Explorer" el fichero de análisis [IEUser_UsrClass.csv](./IEUser_UsrClass.csv). Se aprecian las siguietes actividades:

* Acceso a las carpetas compartidas del equipo.
* Acceso a la unidad de Google Drive.
* Uso del software existente en la ruta "C:\Users\IEUser\AppData\Local\Temp\APTSimulator".
* Acceso a la ruta de red "\\192.168.183.134\192.168.183.134\Docs\archivos".
* Acceso a la ruta "Desktop\Shared Documents Folder (Users Files)\AppData\Local\Temp\dist\APTSimulator".

![Timeline Explorer NTUSER.DAT](./img/24-timeline_explorer_usrclass.png)

#### MFT

Abrir con "Timeline Explorer" el fichero de análisis [20220521091444_MFTECmd_$MFT_Output.csv](./20220521091444_MFTECmd_$MFT_Output.csv). En los últimos registro del fichero se aprecia actividad con los archivos relacionados con el software "APTSimulator":

![Timeline Explorer MFT](./img/25-timeline_explorer_mft.png)

#### PECmd

Abrir con "Timeline Explorer" el fichero de análisis [20220521091508_PECmd_Output.csv](./20220521091508_PECmd_Output.csv). Las entradas más sospechosas parecen las siguientes:

* "7Z.EXE", posiblemente para descomprimir "APTSimulator".
* "TEAMVIEWER.EXE".
* "P.EXE", visto con anterioridad en la carpeta "TMP" como sospechoso.

![Timeline Explorer PECmd](./img/26-timeline_explorer_pecmd.png)

#### PECmd Timeline

Abrir con "Timeline Explorer" el fichero de análisis [20220521091508_PECmd_Output_Timeline.csv](./20220521091508_PECmd_Output_Timeline.csv). Se aprecian actividades similares a las vistas en el punto "PECmd":

![Timeline Explorer PECmd Timeline](./img/27-timeline_explorer_pecmd_timeline.png)

#### SRUM ECmd

Abrir con "Timeline Explorer" el fichero de análisis [20220521091514_SrumECmd_NetworkUsages_Output.csv](./20220521091514_SrumECmd_NetworkUsages_Output.csv). Se aprecian actividades similares a las vistas anteriormente con "Team Viewer", "One Drive" y "Google Drive":

![Timeline Explorer SRUM ECmd](./img/28-timeline_explorer_srum_ecmd.png)

### Estudio de los eventos del sistema

#### Registro de eventos "Windows\System32\winevt\logs\Security.evtx"

Procesar los eventos registrados en el archivo "Windows\System32\winevt\logs\Security.evtx":

```powershell
.\EvtxECmd.exe -f "C:\Users\Equipo Pruebas\Desktop\evidencia\target\E\Windows\System32\winevt\logs\Security.evtx" --csv "C:\Users\Equipo Pruebas\Desktop\evidencia\modules\events" --csvf logon_off_parser.csv
```

![Process Events Logon Off](./img/29-process_events_logon_off.png)

Abrir con "Timeline Explorer" el fichero de análisis [logon_off_parser.csv](./logon_off_parser.csv). Filtrar por:

* Event Id: 4624.
* Payload: "LogonType","#text":"3".

Se puede observar que se ha accedido al equipo aparentemente por SSH:

![Timeline Explorer Logon Off 4624](./img/30-process_events_logon_off_4624_timeline_explorer.png)

#### Registro de eventos "Windows\System32\winevt\logs\Microsoft-Windows-TerminalServices-LocalSessionManager%4Operational.evtx"

Procesar los eventos registrados en el archivo "Windows\System32\winevt\logs\Microsoft-Windows-TerminalServices-LocalSessionManager%4Operational.evtx":

```powershell
.\EvtxECmd.exe -f "C:\Users\Equipo Pruebas\Desktop\evidencia\target\E\Windows\System32\winevt\logs\Microsoft-Windows-TerminalServices-LocalSessionManager%4Operational.evtx" --csv "C:\Users\Equipo Pruebas\Desktop\evidencia\modules\events" --csvf rdp.csv
```

![Process Events RDP](./img/31-process_events_rdp.png)

Abrir con "Timeline Explorer" el fichero de análisis [rdp.csv](./rdp.csv). Se puede observar que se ha accedido al equipo remotamente por RDP:

![Timeline Explorer RDP](./img/32-process_events_rdp_timeline_explorer.png)

### Conclusiones

Tras el estudio de la evidencia se pueden concluir los siguientes puntos:

* Hay indicios de que se ha instalado en el equipo el siguiente software:
  * One Drive (herramienta de almacenamiento en la nube).
  * Google Drive (herramienta de almacenamiento en la nube).
  * Team Viewer (herramienta de acceso remoto).
  * OpenSSH (herramienta de acceso remoto).
  * **APTSimulator (herramienta malware)**.
  * BGInfo (herramienta para obtener información del equipo).
* Se ha accedido remotamente a otros equipos y se han copiado documentos.
* Se ha accedido remotamente a la máquina estudiada.

Con la información aportada en este se podría profundizar para obtener datos más concretos, en función de qué líneas de investigación se deseen seguir.
