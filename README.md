# Digital Forensics and Incident Response - Bootcamp Cybersecurity III

Proyecto académico con el objetivo de aplicar las técnicas y herramientas vistas durante el curso para resolver los ejercicios de la práctica final ([enunciado](./Enunciado_Practica_DFIR.pdf)).

## Contenido

* [Ejercicio 1](./ejercicio_01/README_EJERCICIO_01.md).
* [Ejercicio 2](./ejercicio_02/README_EJERCICIO_02.md).
