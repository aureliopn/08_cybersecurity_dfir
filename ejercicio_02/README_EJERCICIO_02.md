# Digital Forensics and Incident Response - Ejercicio 2 - Metadatos

## Enunciado

La idea de este ejercicio es examinar cómo las plataformas de mensajería quitan una serie de metadatos cuando se envía información entre unas y otras.

Realizar una prueba con una foto del alumno:

1. Ver los metadatos que tiene inicialmente.
2. Enviar por WhatsApp y ver los metadatos resultantes.
3. Enviar por Telegram y ver los metadatos resultantes.
4. Enviar por correo electrónico y ver los metadatos resultantes.

Estos son 3 ejemplos. Se valorará positivamente utilizar otros mecanismos.

## Solución

### Software

Se han utilizado las siguientes herramientas:

* [Windows](https://www.microsoft.com/es-es/software-download/windows10) 10.
  * [VirtualBox](https://www.virtualbox.org/) 6.1.12 r139181 (Qt5.6.2).
    * [Parrot Security](https://www.parrotsec.org/) 5.14.0-9parrot1-amd64.
      * [ExifTool](https://exiftool.org/) 12.16.
  * [TortoiseGitMerge](https://tortoisegit.org/) 2.12.0.0 64 Bit, Mar 31 2021.

### Foto a estudiar

Hacer una foto para realizar el ejercicio:

![Foto Original](./img/01-foto_original.jpg)

Obtener los metadatos:

```text
┌─[user@parrot]─[~/Desktop/practica]
└──╼ $exiftool ./01-foto_original.jpg 
ExifTool Version Number         : 12.16
File Name                       : 01-foto_original.jpg
Directory                       : .
File Size                       : 6.2 MiB
File Modification Date/Time     : 2022:05:22 14:39:00+01:00
File Access Date/Time           : 2022:05:22 14:39:00+01:00
File Inode Change Date/Time     : 2022:05:22 14:39:00+01:00
File Permissions                : rw-rw-rw-
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
Exif Byte Order                 : Big-endian (Motorola, MM)
Camera Model Name               : M2007J20CG
Orientation                     : Rotate 90 CW
Modify Date                     : 2022:05:22 14:46:09
Y Cb Cr Positioning             : Centered
ISO                             : 776
Exposure Program                : Program AE
F Number                        : 1.9
Exposure Time                   : 1/33
Sensing Method                  : Not defined
Sub Sec Time Digitized          : 643869
Sub Sec Time Original           : 643869
Sub Sec Time                    : 643869
Focal Length                    : 5.4 mm
Flash                           : Off, Did not fire
Light Source                    : D65
Metering Mode                   : Center-weighted average
Scene Capture Type              : Standard
Interoperability Index          : R98 - DCF basic file (sRGB)
Interoperability Version        : 0100
Focal Length In 35mm Format     : 25 mm
Max Aperture Value              : 1.9
Create Date                     : 2022:05:22 14:46:09
Exposure Compensation           : 0
Exif Image Height               : 3472
White Balance                   : Auto
Date/Time Original              : 2022:05:22 14:46:09
Brightness Value                : -1.02
Exif Image Width                : 4624
Exposure Mode                   : Auto
Aperture Value                  : 1.9
Components Configuration        : Y, Cb, Cr, -
Color Space                     : sRGB
Scene Type                      : Directly photographed
Shutter Speed Value             : 1/33
Exif Version                    : 0220
Flashpix Version                : 0100
Resolution Unit                 : inches
GPS Latitude Ref                : North
GPS Longitude Ref               : West
GPS Altitude Ref                : Above Sea Level
GPS Time Stamp                  : 12:45:46
GPS Processing Method           : CELLID
GPS Date Stamp                  : 2022:05:22
X Resolution                    : 72
Y Resolution                    : 72
Make                            : Xiaomi
Image Width                     : 4624
Image Height                    : 3472
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:0 (2 2)
Aperture                        : 1.9
Image Size                      : 4624x3472
Megapixels                      : 16.1
Scale Factor To 35 mm Equivalent: 4.6
Shutter Speed                   : 1/33
Create Date                     : 2022:05:22 14:46:09.643869
Date/Time Original              : 2022:05:22 14:46:09.643869
Modify Date                     : 2022:05:22 14:46:09.643869
GPS Altitude                    : 764.2 m Above Sea Level
GPS Date/Time                   : 2022:05:22 12:45:46Z
GPS Latitude                    : 40 deg 28' 49.89" N
GPS Longitude                   : 3 deg 40' 3.58" W
Circle Of Confusion             : 0.007 mm
Field Of View                   : 71.5 deg
Focal Length                    : 5.4 mm (35 mm equivalent: 25.0 mm)
GPS Position                    : 40 deg 28' 49.89" N, 3 deg 40' 3.58" W
Hyperfocal Distance             : 2.39 m
Light Value                     : 3.9
```

### Foto enviada por correo electrónico

Foto resultante de ser enviada por correo electrónico (GMail):

![Foto GMail](./img/02-foto_email.jpg)

Obtener los metadatos:

```text
┌─[user@parrot]─[~/Desktop/practica]
└──╼ $exiftool ./02-foto_email.jpg 
ExifTool Version Number         : 12.16
File Name                       : 02-foto_email.jpg
Directory                       : .
File Size                       : 6.2 MiB
File Modification Date/Time     : 2022:05:22 14:45:47+01:00
File Access Date/Time           : 2022:05:22 14:45:47+01:00
File Inode Change Date/Time     : 2022:05:22 14:45:47+01:00
File Permissions                : rw-rw-rw-
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
Exif Byte Order                 : Big-endian (Motorola, MM)
Camera Model Name               : M2007J20CG
Orientation                     : Rotate 90 CW
Modify Date                     : 2022:05:22 14:46:09
Y Cb Cr Positioning             : Centered
ISO                             : 776
Exposure Program                : Program AE
F Number                        : 1.9
Exposure Time                   : 1/33
Sensing Method                  : Not defined
Sub Sec Time Digitized          : 643869
Sub Sec Time Original           : 643869
Sub Sec Time                    : 643869
Focal Length                    : 5.4 mm
Flash                           : Off, Did not fire
Light Source                    : D65
Metering Mode                   : Center-weighted average
Scene Capture Type              : Standard
Interoperability Index          : R98 - DCF basic file (sRGB)
Interoperability Version        : 0100
Focal Length In 35mm Format     : 25 mm
Max Aperture Value              : 1.9
Create Date                     : 2022:05:22 14:46:09
Exposure Compensation           : 0
Exif Image Height               : 3472
White Balance                   : Auto
Date/Time Original              : 2022:05:22 14:46:09
Brightness Value                : -1.02
Exif Image Width                : 4624
Exposure Mode                   : Auto
Aperture Value                  : 1.9
Components Configuration        : Y, Cb, Cr, -
Color Space                     : sRGB
Scene Type                      : Directly photographed
Shutter Speed Value             : 1/33
Exif Version                    : 0220
Flashpix Version                : 0100
Resolution Unit                 : inches
GPS Latitude Ref                : North
GPS Longitude Ref               : West
GPS Altitude Ref                : Above Sea Level
GPS Time Stamp                  : 12:45:46
GPS Processing Method           : CELLID
GPS Date Stamp                  : 2022:05:22
X Resolution                    : 72
Y Resolution                    : 72
Make                            : Xiaomi
Image Width                     : 4624
Image Height                    : 3472
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:0 (2 2)
Aperture                        : 1.9
Image Size                      : 4624x3472
Megapixels                      : 16.1
Scale Factor To 35 mm Equivalent: 4.6
Shutter Speed                   : 1/33
Create Date                     : 2022:05:22 14:46:09.643869
Date/Time Original              : 2022:05:22 14:46:09.643869
Modify Date                     : 2022:05:22 14:46:09.643869
GPS Altitude                    : 764.2 m Above Sea Level
GPS Date/Time                   : 2022:05:22 12:45:46Z
GPS Latitude                    : 40 deg 28' 49.89" N
GPS Longitude                   : 3 deg 40' 3.58" W
Circle Of Confusion             : 0.007 mm
Field Of View                   : 71.5 deg
Focal Length                    : 5.4 mm (35 mm equivalent: 25.0 mm)
GPS Position                    : 40 deg 28' 49.89" N, 3 deg 40' 3.58" W
Hyperfocal Distance             : 2.39 m
Light Value                     : 3.9
```

### Foto enviada por WhatsApp

Foto resultante de ser enviada por WhatsApp:

![Foto WhatsApp](./img/03-foto_whatsapp.jpg)

Obtener los metadatos:

```text
┌─[user@parrot]─[~/Desktop/practica]
└──╼ $exiftool ./03-foto_whatsapp.jpg 
ExifTool Version Number         : 12.16
File Name                       : 03-foto_whatsapp.jpg
Directory                       : .
File Size                       : 52 KiB
File Modification Date/Time     : 2022:05:22 14:51:40+01:00
File Access Date/Time           : 2022:05:22 14:51:40+01:00
File Inode Change Date/Time     : 2022:05:22 14:51:40+01:00
File Permissions                : rw-rw-rw-
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
JFIF Version                    : 1.01
Resolution Unit                 : None
X Resolution                    : 1
Y Resolution                    : 1
Image Width                     : 1201
Image Height                    : 1600
Encoding Process                : Progressive DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:0 (2 2)
Image Size                      : 1201x1600
Megapixels                      : 1.9
```

### Foto enviada por Telegram

Foto resultante de ser enviada por Telegram:

![Foto Telegram](./img/04-foto_telegram.jpg)

Obtener los metadatos:

```text
┌─[user@parrot]─[~/Desktop/practica]
└──╼ $exiftool ./04-foto_telegram.jpg 
ExifTool Version Number         : 12.16
File Name                       : 04-foto_telegram.jpg
Directory                       : .
File Size                       : 64 KiB
File Modification Date/Time     : 2022:05:22 14:59:16+01:00
File Access Date/Time           : 2022:05:22 14:59:16+01:00
File Inode Change Date/Time     : 2022:05:22 14:59:16+01:00
File Permissions                : rw-rw-rw-
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
JFIF Version                    : 1.01
Resolution Unit                 : inches
X Resolution                    : 72
Y Resolution                    : 72
Profile CMM Type                : 
Profile Version                 : 2.1.0
Profile Class                   : Display Device Profile
Color Space Data                : RGB
Profile Connection Space        : XYZ
Profile Date Time               : 0000:00:00 00:00:00
Profile File Signature          : acsp
Primary Platform                : Unknown ()
CMM Flags                       : Not Embedded, Independent
Device Manufacturer             : 
Device Model                    : 
Device Attributes               : Reflective, Glossy, Positive, Color
Rendering Intent                : Media-Relative Colorimetric
Connection Space Illuminant     : 0.9642 1 0.82491
Profile Creator                 : 
Profile ID                      : 0
Profile Description             : sRGB
Red Matrix Column               : 0.43607 0.22249 0.01392
Green Matrix Column             : 0.38515 0.71687 0.09708
Blue Matrix Column              : 0.14307 0.06061 0.7141
Red Tone Reproduction Curve     : (Binary data 40 bytes, use -b option to extract)
Green Tone Reproduction Curve   : (Binary data 40 bytes, use -b option to extract)
Blue Tone Reproduction Curve    : (Binary data 40 bytes, use -b option to extract)
Media White Point               : 0.9642 1 0.82491
Profile Copyright               : Google Inc. 2016
Image Width                     : 961
Image Height                    : 1280
Encoding Process                : Progressive DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:0 (2 2)
Image Size                      : 961x1280
Megapixels                      : 1.2
```

### Estudio de los metadatos

No hay diferencias entre los metadatos de la foto original y los de la enviada por correo electrónico:

![Diff Original Correo Electrónico](./img/05-diff_original_email.png)

Diferencias exitentes en la foto enviada por WhatsApp respecto a la foto original:

* El tamaño de la imagen se ha reducido.
* La mayor parte de los metadatos han sido eliminados, (por ejemplo, la geolocalización).
* Se ha añadido el metadado "JFIF Version".

![Diff Original WhatsApp](./img/06-diff_original_whatsapp.png)

Diferencias exitentes en la foto enviada por Telegram respecto a la foto original:

* El tamaño de la imagen se ha reducido.
* La mayor parte de los metadatos han sido eliminados, (por ejemplo, la geolocalización).
* Se han añadido metadatos nuevos.

![Diff Original Telegram](./img/07-diff_original_telegram.png)
